//TESTED ON Arduino Mega 2560 R3 -- CHANGED PIN10 to PIN53 in UsbCore.h due to SPI Bus Nomenclature being slightly different among various boards -- 05-05-2017
//compiled on arduino 32u4 bluefruit feather; need proper pinouts for pwm motors and spi pinout for usb host

//Defines
#define ARMSERVOMIN 650
#define ARMSERVOMAX 2350
#define OMNIWHEELMIN 0
#define OMNIWHEELMAX 180

#define PCTINCREMENT 0.01  //used for fine-tuning the rotations
#define PCTTOSERVOLOC 0.17 //this will need to be added to ARMSERVOMIN
#define CONV_FACTOR 6.667  //this is the conversion factor

//pin pwm defines
#define ARMSERVOL  27 //2
#define ARMSERVOR   0 //3
#define OMNIWHEEL1  1 //4
#define OMNIWHEEL2 14 //5
#define OMNIWHEEL3 15 //6
#define OMNIWHEEL4 16 //7

//Headers
#include <PS4BT.h>
#include <Servo.h>
#include <usbhub.h>
#ifdef dobogusinclude
  #include <spi4teensy3.h>
  #include <../../../../hardware/pic32/libraries/SPI/SPI.h> // Hack to use the SPI library
  //#include <SPI.h>
#endif

//Declarations
USB Usb;
BTD Btd(&Usb);
PS4BT PS4(&Btd, PAIR); //force pairing mode for now
//PS4BT PS4(&Btd); //this is for just rejoining the controller; not needed for now

Servo omniWheel1, omniWheel2, omniWheel3, omniWheel4, servoArmLeft, servoArmRight;
int omniWheelSpeed1    = OMNIWHEELMIN+OMNIWHEELMAX/2, omniWheelSpeed2    = OMNIWHEELMIN+OMNIWHEELMAX/2, omniWheelSpeed3     = OMNIWHEELMIN+OMNIWHEELMAX/2, omniWheelSpeed4     = OMNIWHEELMIN+OMNIWHEELMAX/2, armAngle = 0;
int leftStickStateX    = 0, leftStickStateY     = 0, rightStickStateX     = 0, rightStickStateY     = 0;
int oldLeftStickStateX = 0, oldLeftStickStateY  = 0, oldRightStickStateX  = 0, oldRightStickStateY  = 0;
int powerMoveRelay = 19;

unsigned long prevMillis = 0;
unsigned long theInterval = 75;
bool hasMoved = false;
bool isReversed = false;
bool pitchRollActivated = false;
bool touchPadActivated = false;

void setup() {
  delay(5000);
  Serial.begin(115200);
  #if !defined(__MIPSEL__)
    while (!Serial); // Wait for serial port to connect - used on Leonardo, Teensy and other boards with built-in USB CDC serial connection
  #endif
  if (Usb.Init() == -1) {
    Serial.print(F("\r\nOSC did not start. Check physical connections."));
    while (1); //halt
  }
  
  //set pin modes for 2-7 <--- this is probably not needed
  //for(int i = 2; i <= 7; i++) {
  //  pinMode(i, OUTPUT);
  //}

  //activate servos
  //omniWheel1.attach(OMNIWHEEL1, 1000, 2000);
  //omniWheel2.attach(OMNIWHEEL2, 1000, 2000);
  //omniWheel3.attach(OMNIWHEEL3, 1000, 2000);
  //omniWheel4.attach(OMNIWHEEL4, 1000, 2000);
  
  omniWheel1.attach(OMNIWHEEL1);
  omniWheel2.attach(OMNIWHEEL2);
  omniWheel3.attach(OMNIWHEEL3);
  omniWheel4.attach(OMNIWHEEL4);
  
  servoArmLeft.attach(ARMSERVOL);
  servoArmRight.attach(ARMSERVOR);
  
  omniWheel1.write(omniWheelSpeed1);
  omniWheel2.write(omniWheelSpeed2);
  omniWheel3.write(omniWheelSpeed3);
  omniWheel4.write(omniWheelSpeed4);
  
  //omniWheel1.writeMicroseconds(1000);
  //omniWheel2.writeMicroseconds(1000);
  //omniWheel3.writeMicroseconds(1000);
  //omniWheel4.writeMicroseconds(1000);
  
  servoArmLeft.writeMicroseconds(ARMSERVOMIN+ARMSERVOMAX/2);
  servoArmRight.writeMicroseconds(ARMSERVOMIN+ARMSERVOMAX/2);
  Serial.print(F("\r\nUSB Host, controller, and servos initiated."));
}

void loop() {
  Usb.Task();
  unsigned long currentMillis = millis();
  if(currentMillis - prevMillis > theInterval) {
    prevMillis = currentMillis;
    if(PS4.connected()) {
      //currently gives lee way of +/-5 for preventing of accidental movements, very sensitive!!
      //0-255 are the return numbers
      if (PS4.getAnalogHat(LeftHatX) > 132 || PS4.getAnalogHat(LeftHatX) < 122 || PS4.getAnalogHat(LeftHatY) > 132 || PS4.getAnalogHat(LeftHatY) < 122 || PS4.getAnalogHat(RightHatX) > 132 || PS4.getAnalogHat(RightHatX) < 122 || PS4.getAnalogHat(RightHatY) > 132 || PS4.getAnalogHat(RightHatY) < 122) {
        /***************************LEFT HAT CONTROL*****************************/
        /************************************************************************/
        //0: all the way LEFT; 255: all the way RIGHT  PS4.getAnalogHat(LeftHatX);
        if(PS4.getAnalogHat(LeftHatX) >= 0 && PS4.getAnalogHat(LeftHatX) <= 127) {
          //rotate servos to go LEFT
          if((float)PS4.getAnalogHat(LeftHatX)/127 <= 0.5) {
            //move both directional servos to the LEFT
            servoArmLeft.writeMicroseconds((int)ARMSERVOMIN + (PS4.getAnalogHat(LeftHatX) * CONV_FACTOR));
            servoArmRight.writeMicroseconds((int)ARMSERVOMIN + (PS4.getAnalogHat(LeftHatX) * CONV_FACTOR));
          }
          else {
            //move one directional servo to the LEFT
            servoArmLeft.writeMicroseconds((int)ARMSERVOMIN + (PS4.getAnalogHat(LeftHatX) * CONV_FACTOR));
          }
        }
        else {
          if(PS4.getAnalogHat(LeftHatX) > 127 && PS4.getAnalogHat(LeftHatX) <= 255) {
            //rotate servos to go RIGHT
            if((float)PS4.getAnalogHat(LeftHatX)/255 >= 0.75) {
              //move both directional servos to the RIGHT
              servoArmLeft.writeMicroseconds((int)ARMSERVOMIN + (PS4.getAnalogHat(LeftHatX) * CONV_FACTOR));
              servoArmRight.writeMicroseconds((int)ARMSERVOMIN + (PS4.getAnalogHat(LeftHatX) * CONV_FACTOR));
            }
            else {
              //move one directional servo to the RIGHT
              servoArmRight.writeMicroseconds((int)ARMSERVOMIN + (PS4.getAnalogHat(LeftHatX) * CONV_FACTOR));
            }
          }
        }

        //0: all the way UP; 255: all the way DOWN  PS4.getAnalogHat(LeftHatY);
        if(PS4.getAnalogHat(LeftHatY) >= 0 && PS4.getAnalogHat(LeftHatY) <= 127) {
          //rotate servos to go UP
          if((float)PS4.getAnalogHat(LeftHatY)/127 <= 0.5) {
            //move both directional servos to the UP
            servoArmLeft.writeMicroseconds((int)ARMSERVOMIN + (PS4.getAnalogHat(LeftHatY) * CONV_FACTOR));
            servoArmRight.writeMicroseconds((int)ARMSERVOMIN + (PS4.getAnalogHat(LeftHatY) * CONV_FACTOR));
          }
          else {
            //move one directional servo to the UP
            servoArmLeft.writeMicroseconds((int)ARMSERVOMIN + (PS4.getAnalogHat(LeftHatY) * CONV_FACTOR));
          }
        }
        else {
          if(PS4.getAnalogHat(LeftHatY) > 127 && PS4.getAnalogHat(LeftHatY) <= 255) {
            //rotate servos to go DOWN
            if((float)PS4.getAnalogHat(LeftHatY)/255 >= 0.75) {
              //move both directional servos to the DOWN
              servoArmLeft.writeMicroseconds((int)ARMSERVOMIN + (PS4.getAnalogHat(LeftHatY) * CONV_FACTOR));
              servoArmRight.writeMicroseconds((int)ARMSERVOMIN + (PS4.getAnalogHat(LeftHatY) * CONV_FACTOR));
            }
            else {
              //move one directional servo to the DOWN
              servoArmRight.writeMicroseconds((int)ARMSERVOMIN + (PS4.getAnalogHat(LeftHatY) * CONV_FACTOR));
            }
          }
        }
        /***********************END LEFT HAT CONTROL*****************************/
        /************************************************************************/
        
        /**************************RIGHT HAT CONTROL*****************************/
        /************************************************************************/
        //0: all the way LEFT; 255: all the way RIGHT  PS4.getAnalogHat(RightHatX);
        if(PS4.getAnalogHat(RightHatX) >= 0 && PS4.getAnalogHat(RightHatX) <= 127) {
          //rotate servos to go LEFT
          if((float)PS4.getAnalogHat(RightHatX)/127 <= 0.5) {
            //move both directional servos to the LEFT
          }
          else {
            //move one directional servo to the LEFT
          }
        }
        else {
          if(PS4.getAnalogHat(RightHatX) > 127 && PS4.getAnalogHat(RightHatX) <= 255) {
            //rotate servos to go RIGHT
            if((float)PS4.getAnalogHat(RightHatX)/255 >= 0.75) {
              //move both directional servos to the RIGHT
            }
            else {
              //move one directional servo to the RIGHT
            }
          }
        }

        //0: all the way UP; 255: all the way DOWN  PS4.getAnalogHat(RightHatY);
        if(PS4.getAnalogHat(RightHatY) >= 0 && PS4.getAnalogHat(RightHatY) <= 127) {
          //rotate servos to go UP
          if((float)PS4.getAnalogHat(RightHatY)/127 <= 0.5) {
            //move both directional servos to the UP
          }
          else {
            //move one directional servo to the UP
          }
        }
        else {
          if(PS4.getAnalogHat(RightHatY) > 127 && PS4.getAnalogHat(RightHatY) <= 255) {
            //rotate servos to go DOWN
            if((float)PS4.getAnalogHat(RightHatY)/255 >= 0.75) {
              //move both directional servos to the DOWN
            }
            else {
              //move one directional servo to the DOWN
            }
          }
        }
        /*************************END RIGHT HAT CONTROL**************************/
        /************************************************************************/
        
      }
      else {
        if(PS4.getAnalogHat(LeftHatX) > 132 || PS4.getAnalogHat(LeftHatX) < 122 || PS4.getAnalogHat(LeftHatY) > 132 || PS4.getAnalogHat(LeftHatY) < 122) {
          //127 out leftHATXY
        }
        else {
          if(PS4.getAnalogHat(RightHatX) > 132 || PS4.getAnalogHat(RightHatX) < 122 || PS4.getAnalogHat(RightHatY) > 132 || PS4.getAnalogHat(RightHatY) < 122) {
            //127 out rightHATXY
          }
        }
      }

      //process button clicks
      if(PS4.getButtonClick(PS)) {
        //reset the robot, button?
      }
      else {
        //TRIANGLE grab inverse button
        if(PS4.getButtonClick(TRIANGLE)) {
          isReversed = !isReversed;
        }
        //CIRCLE
        if(PS4.getButtonClick(CIRCLE)) {
          //
        }
        //CROSS
        if(PS4.getButtonClick(CROSS)) {
          //
        }
        //SQUARE
        if(PS4.getButtonClick(SQUARE)) {
          //
        }
        //UP
        if(PS4.getButtonClick(UP)) {
          //
        }
        //DOWN
        if(PS4.getButtonClick(DOWN)) {
          //
        }
        //LEFT
        if(PS4.getButtonClick(LEFT)) {
          //
        }
        //RIGHT
        if(PS4.getButtonClick(RIGHT)) {
          //
        }
        //L1
        if(PS4.getButtonClick(L1)) {
          //
        }
        
        //L2 TODO
        
        
        //L3
        if(PS4.getButtonClick(L3)) {
          //
        }
        //R1
        if(PS4.getButtonClick(R1)) {
          //
        }
        
        //R2 TODO

        
        //R3
        if(PS4.getButtonClick(R3)) {
          //
        }
        
        //SHARE
        if(PS4.getButtonClick(SHARE)) {
          //here, just in case
        }
        
        //TOUCHPAD
        if(PS4.getButtonClick(TOUCHPAD)) {
          //toggle touchpad requests
          touchPadActivated = !touchPadActivated;
        }
        if(touchPadActivated) {
          //process touchpad values
        }
        
        //PITCH -- ROLL -- aka OPTIONS
        if(PS4.getButtonClick(OPTIONS)) {
          //toggle pitch and roll
          pitchRollActivated = !pitchRollActivated;
        }
        if(pitchRollActivated) {
          //process pitch and roll movements
        }

        //any more buttons to map?
      }
    }
    else {
      omniWheel1.write(omniWheelSpeed1);
      omniWheel2.write(omniWheelSpeed2);
      omniWheel3.write(omniWheelSpeed3);
      omniWheel4.write(omniWheelSpeed4);
      servoArmLeft.writeMicroseconds(ARMSERVOMIN+ARMSERVOMAX/2);
      servoArmRight.writeMicroseconds(ARMSERVOMIN+ARMSERVOMAX/2);
    }
  }
}